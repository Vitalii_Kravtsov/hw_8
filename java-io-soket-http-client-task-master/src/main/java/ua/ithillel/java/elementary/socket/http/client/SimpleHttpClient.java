package ua.ithillel.java.elementary.socket.http.client;

import java.util.Map;

public interface SimpleHttpClient {
    SimpleHttpResponse request(HttpMethod method, String url, String payload, Map<String, String> headers);

    default SimpleHttpResponse get(String url, Map<String, String> headers) {
        return request(HttpMethod.GET, url, null, headers);
    }

    default SimpleHttpResponse post(String url, String payload, Map<String, String> headers) {
        return request(HttpMethod.POST, url, payload, headers);
    }

    default SimpleHttpResponse put(String url, String payload, Map<String, String> headers) {
        return request(HttpMethod.PUT, url, payload, headers);
    }

    default SimpleHttpResponse delete(String url, Map<String, String> headers) {
        return request(HttpMethod.DELETE, url, null, headers);
    }
}
