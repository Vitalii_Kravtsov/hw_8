package ua.ithillel.java.elementary.socket.http.client;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SimpleHttpClientImpl implements SimpleHttpClient{

    private Matcher matcher;

    @Override
    public SimpleHttpResponse request(HttpMethod method, String url, String payload, Map<String, String> headers) {

        String url_regex =
                "(http:(//|(\\\\{2})))?[a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9\\-]+)*(:\\d{1,5})?([/\\\\][a-zA-Z0-9\\-]+)*([/\\\\])?(\\?([a-zA-Z0-9\\-]+=[a-zA-Z0-9\\-]+)(&[a-zA-Z0-9\\-]+=[a-zA-Z0-9\\-]+)*)?";

        if (!url.matches(url_regex))
            throw new IllegalArgumentException("Invalid url");

        // Parsing URL
        String host = this.getHost(url);
        int port = this.getPort(url);
        String path = this.getPath(url);

        try(Socket socket = new Socket(host, port)) {

            PrintStream socketWriter = new PrintStream(socket.getOutputStream());

            socketWriter.printf("%s %s HTTP/1.0\r\n", method, path);

            for (Map.Entry<String, String> header : headers.entrySet())
                socketWriter.printf("%s: %s\r\n", header.getKey(), header.getValue());

            if (payload != null)
                socketWriter.printf("Content-Length: %s\r\n", payload.getBytes(StandardCharsets.UTF_8).length);

            socketWriter.print("\r\n");

            if (payload != null)
                socketWriter.write(payload.getBytes(StandardCharsets.UTF_8));

            socketWriter.flush();

            Scanner socketScanner = new Scanner(socket.getInputStream());

            // Parsing response
            String head = socketScanner.nextLine();

            int statusCode;
            this.matcher = Pattern.compile(" (\\d+) ").matcher(head);
            this.matcher.find();
            statusCode = Integer.parseInt(this.matcher.group(1));

            String statusText;
            this.matcher = Pattern.compile(" ([^\\d]+)").matcher(head);
            this.matcher.find();
            statusText = this.matcher.group(1);

            Map<String, String> responseHeaders = new HashMap<>();

            while (socketScanner.hasNextLine()) {
                String line = socketScanner.nextLine();
                if (line.isBlank()) break;
                responseHeaders.put(line.split(": ")[0], line.split(": ")[1]);
            }

            StringBuilder responsePayload = null;

            if (socketScanner.hasNextLine()) {
                responsePayload = new StringBuilder();
                while (socketScanner.hasNextLine())
                    responsePayload.append(socketScanner.nextLine()).append("\n");
            }

            return new SimpleHttpResponse(
                    statusCode, statusText, responsePayload == null ? null : responsePayload.toString(), responseHeaders
            );

        } catch (IOException exception) {
            return new SimpleHttpResponse(500, "Internal Server Error", "Internal Server Error", null);
        }

    }

    private String getHost(String url) {

        if (url.startsWith("http"))
            this.matcher = Pattern.compile("[/\\\\]{2}([a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9\\-]+)*)").matcher(url);
        else this.matcher = Pattern.compile("([a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9\\-]+)*)").matcher(url);
        this.matcher.find();

        return this.matcher.group(1);

    }

    private int getPort(String url) {

        this.matcher = Pattern.compile(":(\\d+)").matcher(url);

        return this.matcher.find() ? Integer.parseInt(this.matcher.group(1)) : 80;

    }

    private String getPath(String url) {

        this.matcher = Pattern.compile("(?!/).[/\\\\]([a-zA-Z0-9\\-]+([/\\\\][a-zA-Z0-9\\-]+)*)").matcher(url);

        return this.matcher.find() ? "/" + this.matcher.group(1) : "/";

    }

}
