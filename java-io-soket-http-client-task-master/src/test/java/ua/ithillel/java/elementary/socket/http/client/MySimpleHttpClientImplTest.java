package ua.ithillel.java.elementary.socket.http.client;

import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@WireMockTest(httpPort = 1234)
public class MySimpleHttpClientImplTest {

    @Test
    void shouldMakeGetRequestWithParams() {

        stubTest1();
        SimpleHttpClient client = new SimpleHttpClientImpl();
        SimpleHttpResponse resp = client.get(
                "http://127.0.0.1:1234/get-request-1?param1=test1&param2=test2",
                Map.of("x-test-request-header","hv")
        );
        System.out.println(resp);
        assertEquals(200,resp.getStatusCode());
        assertEquals("OK",resp.getStatusText());
        assertEquals("hello world",resp.getPayload().trim());
        assertEquals("header-value-1",resp.getHeaders().get("x-test-header-1"));
        assertEquals("header-value-2",resp.getHeaders().get("x-test-header-2"));

    }

    @Test
    public void testNotFound() {

        stubTest1();
        SimpleHttpClient client = new SimpleHttpClientImpl();
        SimpleHttpResponse resp = client.get(
                "http://127.0.0.1:1234/get-request1",
                Map.of("x-test-request-header","hv")
        );

        System.out.println(resp);

        assertEquals(404, resp.getStatusCode());
        assertEquals("Not Found", resp.getStatusText());
        assertEquals("text/plain",resp.getHeaders().get("Content-Type"));

    }

    @Test
    public void testInvalidUrl() {

        stubTest1();

        SimpleHttpClient client = new SimpleHttpClientImpl();

        assertThrows(IllegalArgumentException.class, () -> client.get(
                "https://127.0.0.1:1234/get-request-1",
                Map.of("x-test-request-header","hv")
        ));

        assertThrows(IllegalArgumentException.class, () -> client.get(
                "http://127.0.0.1:1234/get-request-1//",
                Map.of("x-test-request-header","hv")
        ));

        assertThrows(IllegalArgumentException.class, () -> client.get(
                "http://127.0.0.1:1234/get-request-1?a=5&b:=3",
                Map.of("x-test-request-header","hv")
        ));

    }

    void stubTest1() {
        stubFor(get("/get-request-1")
                .withHeader("x-test-request-header",equalTo("hv"))
                .willReturn(
                        ok("hello world")
                                .withHeader("x-test-header-1","header-value-1")
                                .withHeader("x-test-header-2","header-value-2")

                ));
    }

}
