package chatroulette.model.server;

import chatroulette.Config;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


@AllArgsConstructor
public class Chat implements Runnable {

    private final Socket client1;
    private final Socket client2;

    @Override
    public void run() {

        try (
                PrintWriter writer1 = new PrintWriter(this.client1.getOutputStream());
                Scanner scanner1 = new Scanner(this.client1.getInputStream());
                PrintWriter writer2 = new PrintWriter(this.client2.getOutputStream());
                Scanner scanner2 = new Scanner(this.client2.getInputStream())
        ) {

            System.err.printf("New chat created (#%s).\n", this.hashCode());

            String message1;
            String message2;

            writer1.printf("Чат начат. (Введите \"%s\" для выхода).\n", Config.CLOSE_CHAT_MARKER);
            writer1.flush();

            writer2.printf("Чат начат. (Введите \"%s\" для выхода).\n", Config.CLOSE_CHAT_MARKER);
            writer2.flush();

            writer1.println(Config.FIRST_WRITER_MARKER);
            writer1.flush();

            while (true) {

                long startWaiting = System.nanoTime();
                while (!scanner1.hasNext()) {
                    if ((System.nanoTime() - startWaiting) / Math.pow(10, 6) > Config.RESPONSE_TIMEOUT) {
                        client1.close();
                        client2.close();
                        return;
                    }
                }
                message1 = scanner1.nextLine();
                if (message1.equals(Config.CLOSE_CHAT_MARKER)) {
                    writer2.println(message1);
                    writer2.flush();
                    break;
                }
                writer2.println("Собеседник: " + message1);
                writer2.flush();

                startWaiting = System.nanoTime();
                while (!scanner2.hasNext()) {
                    if ((System.nanoTime() - startWaiting) / Math.pow(10, 6) > Config.RESPONSE_TIMEOUT) {
                        client1.close();
                        client2.close();
                        return;
                    }
                }
                message2 = scanner2.nextLine();
                if (message2.equals(Config.CLOSE_CHAT_MARKER)) {
                    writer1.println(message2);
                    writer1.flush();
                    break;
                }
                writer1.println("Собеседник: " + message2);
                writer1.flush();

            }

            this.client1.close();
            this.client2.close();

        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        } finally {
            System.err.printf("Chat #%s closed.\n", this.hashCode());
        }

    }

}
