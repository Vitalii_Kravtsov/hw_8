package chatroulette.model.server;

import chatroulette.Config;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Server {

    private final InetAddress host;
    private final int port;

    private Socket waiting;

    private void run() throws BindException {

        try(ServerSocket serverSocket = new ServerSocket(this.port, Config.DEFAULT_SERVER_SOCKET_BACKLOG, this.host)) {

            System.err.printf("Server started at %s:%s.\n", this.host.getHostAddress(), this.port);

            while (true) {

                Socket newClient = serverSocket.accept();

                System.err.println("New client connected.");

                if (this.waiting == null) {
                    this.waiting = newClient;
                } else {
                    new Thread(new Chat(this.waiting, newClient)).start();
                    this.waiting = null;
                }

            }

        } catch (BindException exception) {
            throw exception;
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }

    }

    public static void run(InetAddress host, int port) throws BindException {
        new Server(host, port).run();
    }

}
