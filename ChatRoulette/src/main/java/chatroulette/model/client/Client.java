package chatroulette.model.client;

import chatroulette.Config;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Client {

    private final Scanner scanner;

    private final String host;
    private final int port;

    private void run() throws IOException {

        try(Socket clientSocket = new Socket(this.host, this.port)) {

            try(
                    PrintWriter socketWriter = new PrintWriter(clientSocket.getOutputStream());
                    Scanner socketScanner = new Scanner(clientSocket.getInputStream())
            ) {

                System.out.println("Ожидайте собеседника...");

                while (!socketScanner.hasNext()) {}

                System.out.println(socketScanner.nextLine() + "\n");

                while (!clientSocket.isClosed()) {

                    long startWaiting = System.nanoTime();

                    while (!socketScanner.hasNext()) {
                        if ((System.nanoTime() - startWaiting) / Math.pow(10, 6) > Config.RESPONSE_TIMEOUT) {
                            System.out.printf("Превышено время ожидания ответа (%s мс).\n", Config.RESPONSE_TIMEOUT);
                            clientSocket.close();
                            return;
                        }
                    }

                    String received = socketScanner.nextLine();

                    if (received.equals(Config.CLOSE_CHAT_MARKER)) {
                        clientSocket.close();
                        System.out.println("Собеседник покинул чат.");
                        break;
                    }

                    if (!received.equals(Config.FIRST_WRITER_MARKER))
                        System.out.println(received);

                    System.out.print("Я: ");
                    String toSend = scanner.nextLine();
                    socketWriter.println(toSend);
                    socketWriter.flush();

                    if (toSend.equals(Config.CLOSE_CHAT_MARKER)) {
                        clientSocket.close();
                        System.out.println("Вы покинули чат.");
                    }

                }

            }

        }

    }

    public static void run(Scanner scanner, String host, int port) throws ConnectException {

        try {

            Client client = new Client(scanner, host, port);

            int option;

            while (true) {

                System.out.println("\nВыберите опцию:\n\t1. Начать новый чат.\n\t0. Выйти.\n");

                System.out.print("Введите номер опции: ");

                try {

                    option = scanner.nextInt();

                    scanner.nextLine();

                    if (option == 0)
                        break;
                    else if (option == 1)
                        client.run();
                    else System.out.println("\nНеправильно введена опция.\n");

                } catch (InputMismatchException exception) {
                    scanner.nextLine();
                    System.out.println("\nНеправильно введена опция.\n");
                }


            }

        } catch (ConnectException exception) {
            throw exception;
        } catch (IOException exception) {
            System.out.printf("Ошибка подключения к серверу по адресу %s:%s.\n\n", host, port);
        }

    }

}
