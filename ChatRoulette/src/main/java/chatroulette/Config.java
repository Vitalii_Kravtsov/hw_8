package chatroulette;


public class Config {

    public static final int DEFAULT_SERVER_SOCKET_BACKLOG = 50;

    public static final long RESPONSE_TIMEOUT = 30000;

    public static final String FIRST_WRITER_MARKER = "f";
    public static final String CLOSE_CHAT_MARKER = "exit";

}
