package chatroulette;

import chatroulette.ui.Menu;
import chatroulette.ui.items.*;

import java.util.Scanner;


public class Main {

    private static final Scanner scanner = new Scanner(System.in);

    private static final MenuItem[] items = {
            new RunClientMenuItem(scanner),
            new RunServerMenuItem(scanner),
            new ExitMenuItem()
    };

    private static final Menu menu = new Menu(items, scanner);

    public static void main(String[] args) {
        menu.run();
    }

}
