package chatroulette.ui.items;


public interface MenuItem {

    String getMessage();

    default void run() {}

    default boolean isFinal() {
        return false;
    }

}
