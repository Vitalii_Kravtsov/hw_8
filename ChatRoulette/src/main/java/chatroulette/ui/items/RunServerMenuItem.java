package chatroulette.ui.items;

import chatroulette.model.server.Server;
import lombok.AllArgsConstructor;

import java.net.*;
import java.util.*;


@AllArgsConstructor
public class RunServerMenuItem implements MenuItem {

    private final Scanner scanner;

    @Override
    public String getMessage() {
        return "Сервер";
    }

    @Override
    public void run() {

        InetAddress[] availableHosts;

        try {
            availableHosts = this.getAvailableHosts();
        } catch (SocketException exception) {
            System.out.println("\nОшибка чтения сетевых интерфейсов.\n");
            return;
        }

        while (true) {

            System.out.println("\nВыберите хост для запуска сервера:\n");
            for (int i = 0; i < availableHosts.length; i++)
                System.out.printf("\t%s. %s\n", i + 1, availableHosts[i].getHostAddress());
            System.out.println();

            System.out.print("Введите номер хоста: ");
            int hostNumber;
            while (true) {
                try {
                    hostNumber = scanner.nextInt();
                    if (hostNumber < 1 || hostNumber > availableHosts.length) {
                        scanner.nextLine();
                        System.out.print("Неправильный номер хоста. Введите еще раз: ");
                    } else break;
                } catch (InputMismatchException exception) {
                    scanner.nextLine();
                    System.out.print("Неправильный номер хоста. Введите еще раз: ");
                }
            }

            System.out.print("Введите порт: ");
            int port;
            while (true) {
                try {
                    port = scanner.nextInt();
                    if (!this.validatePort(port)) {
                        scanner.nextLine();
                        System.out.print("Введен несуществующий порт. Введите еще раз: ");
                    } else if (this.isSystemPort(port)) {
                        scanner.nextLine();
                        System.out.print("Это системный порт. Введите еще раз: ");
                    } else break;
                } catch (InputMismatchException exception) {
                    scanner.nextLine();
                    System.out.print("Неправильно введен порт. Введите еще раз: ");
                }
            }

            try {
                Server.run(availableHosts[hostNumber-1], port);
                break;
            } catch (BindException exception) {
                System.out.printf(
                        "\nПорт %s уже занят на %s. Виберите другой. \n\n",
                        port, availableHosts[hostNumber-1].getHostAddress()
                );
                break;
            }

        }

    }

    private InetAddress[] getAvailableHosts() throws SocketException {

        List<InetAddress> hosts = new ArrayList<>();

        for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
            Enumeration<InetAddress> inetAddressEnumeration = networkInterface.getInetAddresses();
            if (inetAddressEnumeration.hasMoreElements()) {
                InetAddress host = networkInterface.getInetAddresses().nextElement();
                if (this.isV4(host.getHostAddress()))
                    hosts.add(host);
            }
        }

        return hosts.toArray(new InetAddress[0]);

    }

    private boolean isV4(String ip) {
        return ip.matches("(\\d{1,3}\\.){3}\\d{1,3}");
    }

    private boolean validatePort(int port) {
        return port >= 0 && port <= 65535;
    }

    private boolean isSystemPort(int port) {
        return port >= 0 && port <= 1023;
    }

}
