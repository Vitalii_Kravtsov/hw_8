package chatroulette.ui.items;

import chatroulette.model.client.Client;
import lombok.AllArgsConstructor;

import java.net.ConnectException;
import java.util.InputMismatchException;
import java.util.Scanner;


@AllArgsConstructor
public class RunClientMenuItem implements MenuItem {

    private final Scanner scanner;

    @Override
    public String getMessage() {
        return "Клиент";
    }

    @Override
    public void run() {

        while (true) {

            System.out.print("Введите адрес сервера: ");
            String host = scanner.nextLine();

            System.out.print("Введите порт: ");
            int port;
            while (true) {
                try {
                    port = scanner.nextInt();
                    if (!this.validatePort(port)) {
                        scanner.nextLine();
                        System.out.print("Введен несуществующий порт. Введите еще раз: ");
                    } else if (this.isSystemPort(port)) {
                        scanner.nextLine();
                        System.out.print("Это системный порт. Введите еще раз: ");
                    } else break;
                } catch (InputMismatchException exception) {
                    scanner.nextLine();
                    System.out.print("Неправильно введен порт. Введите еще раз: ");
                }
            }

            try {
                Client.run(scanner, host, port);
                break;
            } catch (ConnectException exception) {
                System.out.printf("\nНет работающего сервера по адресу %s:%s\n\n", host, port);
            }

        }

    }

    private boolean validatePort(int port) {
        return port >= 0 && port <= 65535;
    }

    private boolean isSystemPort(int port) {
        return port >= 0 && port <= 1023;
    }

}
