package chatroulette.ui;

import chatroulette.ui.items.MenuItem;
import lombok.RequiredArgsConstructor;

import java.util.InputMismatchException;
import java.util.Scanner;


@RequiredArgsConstructor
public class Menu {

    private final MenuItem[] items;
    private final Scanner scanner;

    private void display() {

        System.out.println("Выберите режим работы приложения:");

        for (int i = 0; i < this.items.length; i++) {
            System.out.printf("\t%d. %s\n", i + 1, this.items[i].getMessage());
        }

    }

    private int choose() {

        System.out.print("\nВведите номер опции: ");

        int option = scanner.nextInt();

        scanner.nextLine();

        return option;

    }

    private boolean validate(int option) {
        return option > 0 && option <= this.items.length;
    }

    private boolean runItem(int option) {

        if (this.validate(option)) {

            MenuItem item = this.items[option-1];

            item.run();

            return item.isFinal();

        } else {

            System.out.println("\nНеправильно введена опция.\n");

            return false;

        }

    }

    public void run() {

        boolean exit = false;

        while (!exit) {

            this.display();

            try {
                exit = this.runItem(this.choose());
            } catch (InputMismatchException ignored) {

                scanner.nextLine();

                System.out.println("\nНеправильно введена опция.\n");

            }

        }

    }

}
