package chatroulette.ui;

import chatroulette.ui.items.*;
import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;


public class TestMenu {

    private final Scanner scanner = new Scanner(System.in);

    @Test
    public void testRunClientMenuItemMessage() {
        assertEquals("Клиент", new RunClientMenuItem(this.scanner).getMessage());
    }

    @Test
    public void testRunServerMenuItemMessage() {
        assertEquals("Сервер", new RunServerMenuItem(this.scanner).getMessage());
    }

    @Test
    public void testExitMenuItemMessage() {
        assertEquals("Выход", new ExitMenuItem().getMessage());
    }

}
