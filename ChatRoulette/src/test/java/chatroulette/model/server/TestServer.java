package chatroulette.model.server;

import org.junit.Test;

import java.net.BindException;
import java.net.InetAddress;

import static org.junit.Assert.*;


public class TestServer {

    @Test
    public void testInvalidHost() {

        try {

            InetAddress host = InetAddress.getByName("127.255.255.255");
            int port = 2205;

            assertThrows(BindException.class, () -> Server.run(host, port));

        } catch (Exception exception) {
            throw new AssertionError("Test failed with exception " + exception.getClass());
        }

    }

    @Test
    public void testInvalidPort() {

        try {

            InetAddress host = InetAddress.getLocalHost();

            assertThrows(IllegalArgumentException.class, () -> Server.run(host, -1));
            assertThrows(IllegalArgumentException.class, () -> Server.run(host, 65536));

        } catch (Exception exception) {
            throw new AssertionError("Test failed with exception " + exception.getClass());
        }

    }

    @Test
    public void testPortInUse() {

        try {

            InetAddress host = InetAddress.getLocalHost();

            Thread thread = new Thread(() -> {
                try {
                    Server.run(host, 10500);
                } catch (BindException e) {
                    e.printStackTrace();
                }
            });

            thread.start();

            assertThrows(BindException.class, () -> Server.run(host, 10500));

            thread.stop();

        } catch (Exception exception) {
            throw new AssertionError("Test failed with exception " + exception.getClass());
        }

    }

}
